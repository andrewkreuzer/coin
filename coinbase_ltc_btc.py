import cbpro, time
import datetime as dt
from pymongo import MongoClient

mongo_client = MongoClient('mongodb://mongo:27017/')
db = mongo_client.crypto_database
LTC_BTC = db.LTC_BTC


class CoinBaseWebsocketClient(cbpro.WebsocketClient):
    def on_open(self):
        self.url = "wss://ws-feed.pro.coinbase.com/"
        self.products = ["LTC-BTC"]
        self.message_count = 0
        print("Lets count the messages!")
    def on_message(self, msg):
        self.message_count += 1
        if 'price' in msg and 'type' in msg:
            print ("Message type:", msg["type"],
                   "\t@ {:.3f}".format(float(msg["price"])))
        LTC_BTC.insert_one(msg)
    def on_close(self):
        print("-- Goodbye! --")

wsClient = CoinBaseWebsocketClient()
wsClient.start()
print(wsClient.url, wsClient.products)

start_time = dt.datetime.now()
one_day = dt.timedelta(days=1)
while (dt.datetime.now() <= start_time + one_day):
    print ("\nmessage_count =", "{} \n".format(wsClient.message_count))
    time.sleep(1)
wsClient.close()