FROM python:3.7.2
WORKDIR /coin
COPY . /coin
RUN pip install --trusted-host pypy.python.org -r requirements.txt

CMD ["python", "coinbase_ltc_btc.py"]